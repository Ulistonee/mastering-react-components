import Photos from "../Photos/Photos.jsx";
import '../Main/Main.css'
import PropTypes from "prop-types";

const Main = ( { photos } ) => {
  return (
    <main>
      <ul className="photos-list" >
        {
          photos.map((photo) => {
            return (
              <Photos key={photo.id} photo={photo} />
            )
          })
        }
      </ul>
    </main>
  )
}

Main.propTypes = {
  photos: PropTypes.array
}

export default Main;
