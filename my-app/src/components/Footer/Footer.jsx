import '../Footer/Footer.css'
import PropTypes from "prop-types";

const Footer = ({ title }) => {
  return (
    <footer className="footer">
      <p>{title ? title : 'Footer title'}</p>
    </footer>
  )
}

Footer.propTypes = {
  title: PropTypes.string
}

export default Footer;
