import Header from "../Header/Header.jsx";
import Footer from "../Footer/Footer.jsx";
import {Fragment} from "react";
import PropTypes from 'prop-types';

const Page = ({ children }) => {
  return (
    <Fragment>
      <Header title="Header"/>
        {children}
      <Footer title="Footer"/>
    </Fragment>
  )
}

Page.propTypes = {
  children: PropTypes.element.isRequired
}

export default Page;
