import '../Header/Header.css';
import PropTypes from "prop-types";

const Header = ({ title }) => {
  return (
    <header className="header">
      <p>{title ? title : "Header title"}</p>
    </header>
  )
}

Header.propTypes = {
  title: PropTypes.string
}

export default Header;
