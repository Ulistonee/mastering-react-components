import '../Photos/Photos.css'

const Photos = ({ photo }) => {
  return (
      <li className="photo">
        <img src={photo.url} alt={photo.title}/>
      </li>
  )
}

export default Photos;
