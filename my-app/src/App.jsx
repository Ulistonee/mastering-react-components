import './App.css';
import Page from "./components/Page/Page.jsx";
import Main from "./components/Main/Main";

import {data} from "./data";

let LIMIT = 10;


function getPhotos(data) {
  let result = [];

  for (let i = 0; i < LIMIT; i++) {
    result.push(data[i]);
  }
  return result;
}


function App() {
  return (
    <Page >
      <Main photos={getPhotos(data)}/>
    </Page>
  );
}

export default App;
